from django.test import TestCase, SimpleTestCase, Client
from django.urls import resolve, reverse
from django.utils import timezone
from django.conf import settings
from importlib import import_module
import datetime
from unittest import mock

import pytz
import time

from story7.views import profile


class TestStory7(TestCase):

    def test_profile_url_is_exist(self):
        client = Client()
        response = client.get(reverse('story7:profile'))
        self.assertEqual(response.status_code, 200)

    def test_oy_url_is_not_exist(self):
        client = Client()
        response = client.get('/oy/')
        self.assertEqual(response.status_code, 404)

    def test_profile_using_right_template(self):
        response = self.client.get(reverse('story7:profile'))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'profile.html')

    def test_profile_using_right_html_file(self):
        response = self.client.get(reverse('story7:profile'))
        self.assertContains(response, '<title>Profile')

    def test_profile_using_profile_function(self):
        found = resolve(reverse('story7:profile'))
        self.assertEqual(found.func, profile)
    
    def test_profile_has_an_accordion(self):
         response = self.client.get(reverse('story7:profile'))
         content = response.content.decode('utf-8')
         self.assertIn('accordion', content)

    