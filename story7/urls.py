from django.urls import path, include
from story7 import views
from .views import *
from django.shortcuts import render

app_name = 'story7'

urlpatterns = [
    path('profile/', views.profile, name='profile'),
]