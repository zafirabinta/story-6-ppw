from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

from homepage.models import HomeModel
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.urls import reverse
import time

class TestHomepagePage(StaticLiveServerTestCase):
    
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.browser = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(TestHomepagePage, self).setUp()

    
    def tearDown(self):
        self.browser.close()

    def test_no_projects_alert_is_displayed(self):
        self.browser.get(self.live_server_url)

        alert = self.browser.find_element_by_class_name('judul_dan_form_status')
        self.assertEquals(
            alert.find_element_by_tag_name('h1').text,
            'Halo, apa kabar?'
        )

    def test_button_redirect(self):
        self.browser.get(self.live_server_url)
        
        home_url = self.live_server_url + reverse('homepage:home')
        self.browser.find_element_by_tag_name('span').click()
        self.assertEquals(
            self.browser.current_url,
            home_url
        )

    def test_user_see_new_status(self):
        new_status = HomeModel.objects.create(status = 'tidur')
        self.browser.get(self.live_server_url)

        self.assertEquals(
            self.browser.find_element_by_tag_name('td').text,
            'tidur'
        )