from django.test import TestCase, Client
from django.urls import resolve, reverse
from .views import navigations

# Create your tests here.
class Story9Test(TestCase):

    def test_navigation_url_is_exist(self):
        client = Client()
        response = client.get(reverse('story9:navigations'))
        self.assertEqual(response.status_code, 200)

    def test_oy_url_is_not_exist(self):
        client = Client()
        response = client.get('/oy/')
        self.assertEqual(response.status_code, 404)

    def test_login_using_right_template(self):
        client = Client()
        response = client.get('/accounts/login/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'registration/login.html')

    def test_logout_using_right_template(self):
        client = Client()
        response = client.get('/accounts/logout/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'registration/logged_out.html')

    def test_navigation_using_right_template(self):
        response = self.client.get(reverse('story9:navigations'))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'navigation.html')

    def test_navigation_using_right_html_file(self):
        response = self.client.get(reverse('story9:navigations'))
        self.assertContains(response, '<title>Navigation')

    def test_navigation_using_navigation_function(self):
        found = resolve(reverse('story9:navigations'))
        self.assertEqual(found.func, navigations)

    def test_navigation_has_a_button(self):
        response = self.client.get(reverse('story9:navigations'))
        content = response.content.decode('utf-8')
        self.assertIn('button', content)
    