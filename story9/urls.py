from django.urls import path, include
from story9 import views
from .views import *
from django.shortcuts import render

app_name = 'story9'

urlpatterns = [
    path('navigation/', views.navigations, name='navigations'),
]