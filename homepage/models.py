from django.db import models
from django.utils import timezone
from datetime import datetime, date

# Create your models here.

class HomeModel(models.Model):
    status = models.CharField(max_length=300)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name
