from django.test import TestCase
from homepage.forms import HomeForm


class TestForms(TestCase):

    def test_form_data_is_valid(self):
        form = HomeForm(data={'status': 'tidur'})
        self.assertTrue(form.is_valid())

    def test_form_data_is_not_valid(self):
        form = HomeForm(data={'status': ''})
        self.assertFalse(form.is_valid())
        self.assertEquals(len(form.errors), 1)
        
    def test_form_validation_for_blank_items(self):
            form = HomeForm(data={'status': ''})
            self.assertFalse(form.is_valid())
            self.assertEqual(
                form.errors['status'],
                ["This field is required."]
            )

    def test_status_length_is_exceeded(self):
            form = HomeForm(data={
                'status': 'tidur' * 1000,
            })
            self.assertFalse(form.is_valid())
            
    def test_status_length_is_valid(self):
        form = HomeForm(data={
            'status': 'z' * 300,
        })
        self.assertTrue(form.is_valid())
