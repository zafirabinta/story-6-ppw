from django.test import TestCase, Client
from django.urls import resolve, reverse
from django.utils import timezone
from django.conf import settings
from importlib import import_module
import datetime
from unittest import mock

import pytz
import time

from homepage.views import home, jawaban_status
from homepage.models import HomeModel
import json

class TestViews(TestCase):
    
    def test_home_url_is_exist(self):
        client = Client()
        response = client.get(reverse('homepage:home'))
        self.assertEqual(response.status_code, 200)

    def test_hey_url_is_not_exist(self):
        client = Client()
        response = client.get('/hey/')
        self.assertEqual(response.status_code, 404)

    def test_home_using_right_template(self):
        response = self.client.get(reverse('homepage:home'))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'home.html')

    def test_home_using_right_html_file(self):
        response = self.client.get(reverse('homepage:home'))
        self.assertContains(response, '<title>Home')

    def test_model_can_create_new_status(self):
        new_status = HomeModel.objects.create(status = 'tidur')
        status_count = HomeModel.objects.all().count()
        self.assertEqual(status_count, 1)

    def test_model_can_update_itself(self):
        new_status = HomeModel.objects.create(status="tidur")
        HomeModel.objects.filter(pk=new_status.pk).update(status="makan")
        new_status.refresh_from_db()
        self.assertEqual(new_status.status, "makan")

    def test_model_no_data_added_to_database(self):
        Client().post(reverse('homepage:jawaban_status'), {'status': ''})
        status_count = HomeModel.objects.all().count()
        self.assertEqual(status_count, 0)

    def test_home_post_success_and_render_status(self):
        test_status = 'makan'
        response_post = Client().post(reverse('homepage:jawaban_status'), {'status': test_status})
        self.assertEqual(response_post.status_code, 302)

        response= Client().get(reverse('homepage:home'))
        html_response = response.content.decode('utf8')
        self.assertIn(test_status, html_response)

    def test_home_post_fail_and_render_no_status(self):
        test_status = 'makan'
        response_post = Client().post(reverse('homepage:jawaban_status'), {'status': ''})
        self.assertEqual(response_post.status_code, 302)

        response= Client().get(reverse('homepage:home'))
        html_response = response.content.decode('utf8')
        self.assertNotIn(test_status, html_response)

    def test_model_date_auto_now_add_works(self):
        mocked_time = datetime.datetime(2019, 2, 3, 0, 0, 0, tzinfo=pytz.utc)
        with mock.patch('django.utils.timezone.now', mock.Mock(return_value=mocked_time)):
            new_status = HomeModel.objects.create()
            self.assertEqual(new_status.created_at, mocked_time)
