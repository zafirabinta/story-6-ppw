from django.test import SimpleTestCase
from django.urls import resolve, reverse
from homepage.views import home


class TestUrls(SimpleTestCase):

    def test_home_url_using_def_home(self):
        found = resolve(reverse('homepage:home'))
        self.assertEqual(found.func, home)