from django.urls import path, include
from homepage import views

app_name = 'homepage'

urlpatterns = [
    path('', views.home, name='home'),
    path('status/', views.jawaban_status, name='jawaban_status'),
]