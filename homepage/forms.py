from django import forms
from django.forms import ModelForm
from .models import HomeModel

class HomeForm(ModelForm):
    class Meta:
        model = HomeModel
        fields = ['status']
        labels = {'status' : 'Status :'}
        widgets = {'status' : forms.TextInput(attrs={'class' : 'form-control', 'type' : 'text'})}