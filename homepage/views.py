from django.shortcuts import render, redirect, reverse
from .forms import HomeForm
from .models import HomeModel
from django.contrib import messages
from django.http import HttpResponseRedirect

# Create your views here.

def home(request): 
    form = HomeForm()
    database = HomeModel.objects.all()
    content = {'form':form, 'database':database}
    return render(request, 'home.html', content)

def jawaban_status(request):
    if request.method == 'POST':
        form = HomeForm(request.POST)
        if form.is_valid():
            fields = ['status', 'created_at']
            form.save()

    return redirect(reverse('homepage:home'))
