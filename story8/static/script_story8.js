$(document).ready(function(){
    function cariBuku(){
        var search = $('#input-search').val()
        $('#results').html("")
        console.log(search)    

        $.ajax({
            url: 'https://www.googleapis.com/books/v1/volumes?q=' + search,
            dataType: 'json',
            success: function(data){
                console.log(data);
                var items = data.items;
                for(var i = 0; i < items.length; i++){
                    let buku = items[i].volumeInfo;
                    
                    if ('title' in buku == false) var judul = '<td class="text-center">-</td>';
                    else var judul = '<td class="text-center">' + items[i].volumeInfo.title + '</td>';

                    if ('authors' in buku == false) var penulis = '<td class="text-center">-</td>';
                    else var penulis = '<td class="text-center">' + items[i].volumeInfo.authors + '</td>';

                    if ('publisher' in buku == false) var penerbit = '<td class="text-center">-</td>';
                    else var penerbit = '<td class="text-center">' + items[i].volumeInfo.publisher + '</td>';

                    if ('categories' in buku == false) var kategori = '<td class="text-center">-</td>';
                    else var kategori = '<td class="text-center">' + items[i].volumeInfo.categories + '</td>';

                    if ('imageLinks' in buku == false) var sampul = '<td class="text-center">-</td>';
                    else var sampul = '<td class="text-center"><img src="' + items[i].volumeInfo.imageLinks.smallThumbnail + '"></td>';
                    
                    $('#results').append('<tr>', judul, penulis, penerbit, kategori, sampul, '</tr>')
                }
            },
            type: 'GET'
        });
    }

    $("#button-search").click(function(){
        cariBuku();
        return false;
    });

    $.ajax({
        url: 'https://www.googleapis.com/books/v1/volumes?q=maximumride',
        dataType: 'json',
        success: function(data){
            console.log(data);
            var items = data.items;
            for(var i = 0; i < 10; i++){
                let buku = items[i].volumeInfo;
                    
                if ('title' in buku == false) var judul = '<td class="text-center">-</td>';
                else var judul = '<td class="text-center">' + items[i].volumeInfo.title + '</td>';

                if ('authors' in buku == false) var penulis = '<td class="text-center">-</td>';
                else var penulis = '<td class="text-center">' + items[i].volumeInfo.authors + '</td>';

                if ('publisher' in buku == false) var penerbit = '<td class="text-center">-</td>';
                else var penerbit = '<td class="text-center">' + items[i].volumeInfo.publisher + '</td>';

                if ('categories' in buku == false) var kategori = '<td class="text-center">-</td>';
                else var kategori = '<td class="text-center">' + items[i].volumeInfo.categories + '</td>';

                if ('imageLinks' in buku == false) var sampul = '<td class="text-center">-</td>';
                else var sampul = '<td class="text-center"><img src="' + items[i].volumeInfo.imageLinks.smallThumbnail + '"></td>';
                
                $('#results').append('<tr>', judul, penulis, penerbit, kategori, sampul, '</tr>')
            }
        },
        type: 'GET'
    });
    

});


                // let kumpulanBuku = data.items;
                // for(i = 0; i < 5; i++){
                //     if(kumpulanBuku[i] == undefined){
                //         continue;
                //     }

                //     else{
                //         let infoBuku = kumpulanBuku[i].volumeInfo;
                //         var appendNama = $('#results').text(infoBuku.title);
                //         var appendPenerbit = $('#results').text(infoBuku.publisher);
                //     }
                    
                //     $('.ini').append(appendNama, appendPenerbit);
                //     // results += "<h2>" + data.items[i].volumeInfo.title + "</h2>"
                // }