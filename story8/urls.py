from django.urls import path, include
from story8 import views
from .views import *
from django.shortcuts import render

app_name = 'story8'

urlpatterns = [
    # path('books/', views.call_books, name='call_books'),
    path('list/', views.list_books, name='list_books'),
]