from django.test import TestCase, Client
from django.urls import resolve, reverse
from .views import list_books

# Create your tests here.
class Story8Test(TestCase):
    def test_books_url_is_exist(self):
        client = Client()
        response = client.get(reverse('story8:list_books'))
        self.assertEqual(response.status_code, 200)

    def test_oy_url_is_not_exist(self):
        client = Client()
        response = client.get('/oy/')
        self.assertEqual(response.status_code, 404)

    def test_books_using_right_template(self):
        response = self.client.get(reverse('story8:list_books'))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'books.html')

    def test_books_using_right_html_file(self):
        response = self.client.get(reverse('story8:list_books'))
        self.assertContains(response, '<title>Books')

    def test_books_using_profile_function(self):
        found = resolve(reverse('story8:list_books'))
        self.assertEqual(found.func, list_books)

    def test_books_has_a_table(self):
        response = self.client.get(reverse('story8:list_books'))
        content = response.content.decode('utf-8')
        self.assertIn('table', content)
    