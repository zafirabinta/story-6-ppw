from django.shortcuts import render
from django.shortcuts import redirect
import requests
import json
from django.http import JsonResponse

# Create your views here.
# def call_books(request, keyword):
#     r = requests.get('https://www.googleapis.com/books/v1/volumes?q=' + keyword)
#     return JsonResponse(r.json())

def list_books(request):
    return render(request, 'books.html')